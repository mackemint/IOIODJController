package com.example.ioiodjcontroller

import android.util.Log
import ioio.lib.api.AnalogInput
import ioio.lib.api.DigitalOutput
import ioio.lib.api.IOIO
import ioio.lib.api.exception.ConnectionLostException

class PotScanner(ioio: IOIO, midiOutput: MIDIOutput) : Thread() {

    private val _params: Params = Params()

    /**
     * Used for pausing the thread
     */
    private var _running = false

    /**
     * IOIO from main activity
     */
    private var _ioio = ioio

    private var _midiOutput = midiOutput

    /**
     * Handles the raw values from the analog input
     */
    private var _lpf: Array<Array<PotConditioner?>>

    /**
     * The columns take in the signal from the outputs
     * Array containing columns of analog inputs
     */
    private var _analogInput: Array<AnalogInput?>

    /**
     * The rows output signal
     * Array containing rows of digital outputs
     */
    private var _digitalOutput: Array<DigitalOutput?>


    private var _rowCount = 0

    init {
        _running = true
        _lpf = Array(_outPin.size) {
            arrayOfNulls(
                _inPin.size
            )
        }
        _analogInput = arrayOfNulls(_inPin.size)
        _digitalOutput = arrayOfNulls(_outPin.size)
        var lpfCounter = 0
        try {
            for (i in _outPin.indices) {

                // As a workaround for the async bug, only the output is opened here
                _digitalOutput[i] =
                _ioio.openDigitalOutput(_outPin[i], DigitalOutput.Spec.Mode.NORMAL, true)
                _ioio.sync()

                //Now opening all analog inputs and outputs
                for (j in _inPin.indices) {
                    //Only open the outputs the first round
                    if (i < 1) {
                        _analogInput[j] = _ioio.openAnalogInput(_inPin[j])
                        _analogInput[j]?.setBuffer(100)
                    }
                    _lpf[i][j] = PotConditioner(_analogInput[j]!!.readSync(), i)
                    lpfCounter++
                }
                _digitalOutput[i]?.write(false)
            }
        } catch (e: ConnectionLostException) {
            _running = false
            e.printStackTrace()
        }

        //Increases the priority of the current thread
        currentThread().priority = MAX_PRIORITY
    }


    override fun run() {
        while (_running) {
            try {
                for (i in _outPin.indices) {
                    scanColumns()
                }
                sleep(1)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: ConnectionLostException) {
                _running = false
                Log.i(DEBUG_TAG, "Exited App")
            }
        }
    }

    /**
     * Method for scanning all the analog inputs of a row.
     * Opens one analog input at a time, reads the value and smooths it out for sending to the input handler.
     * After reading an analog input, the pin is closed to force syncing between input and output.

     */
    private fun scanColumns() {
        _digitalOutput[_rowCount]!!.write(true)

        //Syncing before continuing to read
        _ioio.sync()
        for (i in _inPin.indices) {
            val smoothVal = _lpf[_rowCount][i]?.getFilteredInput(_analogInput[i]!!.readSync(), _rowCount)
            if (smoothVal != -1) {
                handleOnPotChanged(_params.getPotCCValue(_rowCount, i), smoothVal)
            }
        }
        _digitalOutput[_rowCount]!!.write(false)
        countRows()
    }

    private fun handleOnPotChanged(ccNumber: Int, smoothVal: Int?) {
        if (smoothVal != null) {
            _midiOutput.addCcToQueue(ccNumber, smoothVal)
        }
    }

    /**
     * Counts up rows and wraps around when reaching the length
     */
    private fun countRows() {
        _rowCount++
        if (_rowCount == _outPin.size) _rowCount = 0
    }


    /**
     * Sets running status
     * @param s - the status
     */
    fun setStatus(s: Boolean) {
        _running = s
    }

    companion object {
        private const val DEBUG_TAG: String = "PotScanner"

        /**
         * The row pins for digital output
         *
         */
        private const val ROW1_PIN = 28 // Top row
        private const val ROW2_PIN = 29
        private const val ROW3_PIN = 30

        /**
         * The column pins for the analog input
         *
         */
        private const val COL1_PIN = 31
        private const val COL2_PIN = 38
        private const val COL3_PIN = 33
        private const val COL4_PIN = 40
        private const val COL5_PIN = 35
        private const val COL6_PIN = 44
        /**
         * Array of pins for analog input
         */
        private val _inPin = intArrayOf(COL1_PIN, COL2_PIN, COL3_PIN, COL4_PIN, COL5_PIN, COL6_PIN)
        /**
         * Array of pins for digital output
         */
        private val _outPin = intArrayOf(ROW1_PIN, ROW2_PIN, ROW3_PIN)
    }

}