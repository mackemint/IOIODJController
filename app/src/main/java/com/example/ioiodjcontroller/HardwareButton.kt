package com.example.ioiodjcontroller

/**
 * Class to handle button presses on keyboard matrix.
 *
 * A hardware button has two states, current and previous.
 * This is how to know if a button is being pressed or released.
 * It also is assigned a note number field.
 *
 * @author djdiskmachine
 */
class HardwareButton(noteNumber: Int) {
    /**
     * Note number assigned to the hardware button
     */
    private var _noteNumber: Int = 0

    /**
     * Hardware buttons' previous state
     */
    private var _previousState: Boolean = false


    /**
     * Hardware buttons' current state
     */
    private var currentState: Boolean = false


    /**
     * @return the _noteNumber
     */
    val getNoteNumber: Int
        get() = _noteNumber

    fun getCurrentState(): Boolean {
        return currentState
    }

    fun setPreviousState(currentState: Boolean) {
        _previousState = currentState
    }

    fun setCurrentState(input: Boolean) {
        currentState = input
    }


    val isPressed: Boolean
        get() = currentState && !_previousState
    val wasPressed: Boolean
        get() = currentState && _previousState
    val isReleased: Boolean
        get() = !currentState && _previousState

    init {
        _noteNumber = noteNumber
    }
}
