package com.example.ioiodjcontroller

import android.util.Log
import ioio.javax.sound.midi.MidiMessage
import ioio.javax.sound.midi.ShortMessage
import ioio.lib.api.exception.ConnectionLostException
import java.io.OutputStream
import java.util.concurrent.ArrayBlockingQueue


class MIDIOutput(outputStream: OutputStream?) : Thread()  {

    private val _outputStream = outputStream

    @Volatile
    private var _running: Boolean = true
    /**
     * Sets running status of the thread
     * @param s - the status
     */
    fun setStatus(s: Boolean) {
        _running = s
    }

    private var outQueue = ArrayBlockingQueue<MidiMessage>(OUT_QUEUE_SIZE)


    /**
     * Adds a MIDI note to the output queue
     *
     * @param note
     * @param vel
     */
    fun addNoteToQueue(note: Int, vel: Int) {
        val msg = ShortMessage()
        val midiCh = 0 // TODO create params class_params.getMIDIChannel()

        try {
            //Log.d(DEBUG_TAG, "output message $midiCh $note $vel")
            msg.setMessage(ShortMessage.NOTE_ON, midiCh, note, vel)
            outQueue.add(msg)
        } catch (e: Exception) {
            Log.e(DEBUG_TAG, "InvalidMidiDataException caught")
        }
    }

    /**
     * Adds a CC value to the output queue
     *
     * @param ccNum
     * @param amount
     */
    fun addCcToQueue(ccNum: Int, amount: Int) {
        val msg = ShortMessage()

        val midiCh = 0 // TODO create params class_params.getMIDIChannel()
        //Log.i(DEBUG_TAG, "Changing CC#: $ccNum to $amount")
        try {
            msg.setMessage(ShortMessage.CONTROL_CHANGE, midiCh, ccNum, amount)
            outQueue.add(msg)
        } catch (e: Exception) {
            Log.e(DEBUG_TAG, "InvalidMidiDataException caught")
        }
    }

    override fun run() {
        while (_running) {
            if(!outQueue.isEmpty())
            {
                try {
                    _outputStream?.write(outQueue.poll().message)
                } catch (e: ConnectionLostException) {
                    _running = false
                    e.printStackTrace()
                } catch (e: InterruptedException) {
                    _running = false
                    e.printStackTrace()
                }
            }
        }
    }

    companion object {
        private const val DEBUG_TAG = "MIDIOutput"

        /**
         * The output queue containing Midi Messages
         */
        private const val OUT_QUEUE_SIZE = 200
    }

}