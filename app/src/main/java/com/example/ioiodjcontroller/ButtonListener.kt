package com.example.ioiodjcontroller

import android.graphics.LightingColorFilter
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Button

class ButtonListener(midiOutput: MIDIOutput): OnTouchListener {

    private val ACTIVE_COLOR = -0xff0100

    private val INIT_COLOR = -0x1000000

    private var _midiOutput = midiOutput

    init {
        _midiOutput = midiOutput
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        //Pad numbers are fetched from the button tag property
        val noteNumber = Integer.valueOf(v.tag.toString())
        try {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> _midiOutput.addNoteToQueue(noteNumber, 1)
                MotionEvent.ACTION_UP -> _midiOutput.addNoteToQueue(noteNumber, 0)
            }
        } catch (e: Exception) {
            println(e)
        }
        return false
    }

    private fun setColor(b: Button, vel: Int) {
        //Gives the button a nice green tint
        val buttonColour = vel * ACTIVE_COLOR
        b.background.colorFilter = LightingColorFilter(INIT_COLOR, buttonColour)
    }
}
