    package com.example.ioiodjcontroller

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.widget.Button
import android.widget.SeekBar
import android.widget.Toast
import ioio.lib.api.DigitalOutput
import ioio.lib.api.IOIO
import ioio.lib.api.IOIO.VersionType
import ioio.lib.api.Uart
import ioio.lib.api.Uart.Parity
import ioio.lib.api.Uart.StopBits
import ioio.lib.api.exception.ConnectionLostException
import ioio.lib.spi.Log
import ioio.lib.util.BaseIOIOLooper
import ioio.lib.util.IOIOLooper
import ioio.lib.util.android.IOIOActivity
import java.io.OutputStream

class MainActivity : IOIOActivity() {

    //Hotcues
    private lateinit var buttonLeftHC1: Button
    private lateinit var buttonLeftHC2: Button
    private lateinit var buttonLeftHC3: Button

    private lateinit var buttonRightHC1: Button
    private lateinit var buttonRightHC2: Button
    private lateinit var buttonRightHC3: Button

    private lateinit var buttonExtraHC1: Button
    private lateinit var buttonExtraHC2: Button
    private lateinit var buttonExtraHC3: Button

    //Loop buttons
    private lateinit var buttonLeft8: Button
    private lateinit var buttonLeft16: Button
    private lateinit var buttonLeft32: Button

    private lateinit var buttonRight8: Button
    private lateinit var buttonRight16: Button
    private lateinit var buttonRight32: Button

    private lateinit var buttonExtra8: Button
    private lateinit var buttonExtra16: Button
    private lateinit var buttonExtra32: Button

    private var buttonArray = arrayOf(
                                buttonLeftHC1,
                                buttonLeftHC2,
                                buttonLeftHC3,
                                buttonRightHC1,
                                buttonRightHC2,
                                buttonRightHC3,
                                buttonExtraHC1,
                                buttonExtraHC2,
                                buttonExtraHC3,
                                buttonLeft8,
                                buttonLeft16,
                                buttonLeft32,
                                buttonRight8,
                                buttonRight16,
                                buttonRight32,
                                buttonExtra8,
                                buttonExtra16,
                                buttonExtra32)   

    private lateinit var seekBendLeft: SeekBar
    private lateinit var seekBendRight: SeekBar
    private lateinit var seekBendExtra: SeekBar

    private var seekArray = arrayOf(seekBendLeft, seekBendRight, seekBendExtra)

    private lateinit var _midiOutput: MIDIOutput
    private lateinit var _buttonListener: ButtonListener
    private lateinit var _seekListener: SeekBarListener


    /**
     * Called when the activity is first created. Here we normally initialize
     * our GUI.
     */
    @SuppressLint("ClickableViewAccessibility")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE

        buttonLeftHC1 = findViewById(R.id.button_left_hc1)
        buttonLeftHC2 = findViewById(R.id.button_left_hc2)
        buttonLeftHC3 = findViewById(R.id.button_left_hc3)

        buttonRightHC1 = findViewById(R.id.button_right_hc1)
        buttonRightHC2 = findViewById(R.id.button_right_hc2)
        buttonRightHC3 = findViewById(R.id.button_right_hc3)

        buttonExtraHC1 = findViewById(R.id.button_extra_hc1)
        buttonExtraHC2 = findViewById(R.id.button_extra_hc2)
        buttonExtraHC3 = findViewById(R.id.button_extra_hc3)

        buttonLeft8 = findViewById(R.id.button_left_8)
        buttonLeft16 = findViewById(R.id.button_left_16)
        buttonLeft32 = findViewById(R.id.button_left_32)

        buttonRight8 = findViewById(R.id.button_right_8)
        buttonRight16 = findViewById(R.id.button_right_16)
        buttonRight32 = findViewById(R.id.button_right_32)

        buttonExtra8 = findViewById(R.id.button_extra_8)
        buttonExtra16 = findViewById(R.id.button_extra_16)
        buttonExtra32 = findViewById(R.id.button_extra_32)

        seekBendLeft = findViewById(R.id.seekbar_bend_left)
        seekBendRight = findViewById(R.id.seekbar_bend_right)
        seekBendExtra = findViewById(R.id.seekbar_bend_extra)

        //TODO to suppress the uninitialized _buttonListener, _seekListener
//        buttonLeftHC1.setOnTouchListener(_buttonListener)
//        buttonLeftHC2.setOnTouchListener(_buttonListener)
//        buttonLeftHC3.setOnTouchListener(_buttonListener)

//        buttonRightHC1.setOnTouchListener(_buttonListener)
//        buttonRightHC2.setOnTouchListener(_buttonListener)
//        buttonRightHC3.setOnTouchListener(_buttonListener)

//        seekBendLeft.setOnSeekBarChangeListener(_seekListener)
//        seekBendRight.setOnSeekBarChangeListener(_seekListener)
//        seekBendExtra.setOnSeekBarChangeListener(_seekListener)

        buttonLeftHC1.tag = 16
        buttonLeftHC2.tag = 17
        buttonLeftHC3.tag = 18

        buttonRightHC1.tag = 19
        buttonRightHC2.tag = 20
        buttonRightHC3.tag = 21

        buttonExtraHC1.tag = 22
        buttonExtraHC2.tag = 23
        buttonExtraHC3.tag = 24

        buttonLeft8.tag = 25
        buttonLeft16.tag = 26
        buttonLeft32.tag = 27

        buttonRight8.tag = 28
        buttonRight16.tag = 29
        buttonRight32.tag = 30

        buttonExtra8.tag = 31
        buttonExtra16.tag = 32
        buttonExtra32.tag = 33

        seekBendLeft.tag = 34
        seekBendRight.tag = 35
        seekBendExtra.tag = 36
}

    /**
     * A method to create our IOIO thread.
     *
     * @see ioio.lib.util.AbstractIOIOActivity.createIOIOThread
     */
    override fun createIOIOLooper(): IOIOLooper {
        return Looper()
    }

    private fun showVersions(ioio: IOIO, title: String) {
        toast(
            String.format(
                "%s\n" +
                        "IOIOLib: %s\n" +
                        "Application firmware: %s\n" +
                        "Bootloader firmware: %s\n" +
                        "Hardware: %s",
                title,
                ioio.getImplVersion(VersionType.IOIOLIB_VER),
                ioio.getImplVersion(VersionType.APP_FIRMWARE_VER),
                ioio.getImplVersion(VersionType.BOOTLOADER_VER),
                ioio.getImplVersion(VersionType.HARDWARE_VER)
            )
        )
    }

    private fun toast(message: String) {
        val context: Context = this
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
    }

    private fun enableUi(enable: Boolean) {
        runOnUiThread {
            for (i in buttonArray){
                i.isEnabled = enable
            }
            for (i in seekArray){
                i.isEnabled = enable
            }
        }
    }

    /**
     * This is the thread on which all the IOIO activity happens. It will be run
     * every time the application is resumed and aborted when it is paused. The
     * method setup() will be called right after a connection with the IOIO has
     * been established (which might happen several times!). Then, loop() will
     * be called repetitively until the IOIO gets disconnected.
     */
    internal inner class Looper(
        private val BAUD: Int = 31250,
        private val MIDI_OUTPUT_PIN: Int = 7,
        private val MIDI_INPUT_PIN: Int = 6) : BaseIOIOLooper() {
        /**
         * The on-board LED.
         */
        private var _led: DigitalOutput? = null
        private var _outputStream: OutputStream? = null
        private var _buttonScanner: ButtonScanner? = null
        private var _potScanner: PotScanner? = null

        /**
         * The output for MIDI messages
         */
        private var _uart: Uart? = null

        /**
         * Called every time a connection with IOIO has been established.
         * Typically used to open pins.
         *
         * @throws ConnectionLostException When IOIO connection is lost.
         * @see ioio.lib.util.IOIOLooper.setup
         */
        @Throws(ConnectionLostException::class)
        override fun setup() {
            Log.d(TAG, "setup!")
            showVersions(ioio_, "IOIO connected!")
            _led = ioio_.openDigitalOutput(0, true)
            _uart = ioio_.openUart(
                MIDI_INPUT_PIN,
                MIDI_OUTPUT_PIN,
                BAUD,
                Parity.NONE,
                StopBits.ONE
            )

            if (_uart != null) {
                _outputStream = _uart!!.outputStream
            }
            _midiOutput = MIDIOutput(_outputStream)
            _buttonScanner = ButtonScanner(ioio_, _midiOutput)
            _potScanner = PotScanner(ioio_, _midiOutput)

            //TODO to suppress the uninitialized midiOutput for now
            _buttonListener = ButtonListener(_midiOutput)
            _seekListener = SeekBarListener(_midiOutput)

            for (i in buttonArray)
                i.setOnTouchListener(_buttonListener)

            for (i in seekArray)
                i.setOnSeekBarChangeListener(_seekListener)

            _midiOutput.start()
            _buttonScanner!!.start()
            _potScanner!!.start()

            enableUi(true)
        }

        /**
         * Called repetitively while the IOIO is connected.
         *
         * @throws ConnectionLostException When IOIO connection is lost.
         * @throws InterruptedException    When the IOIO thread has been interrupted.
         * @see ioio.lib.util.IOIOLooper.loop
         */
        @Throws(ConnectionLostException::class, InterruptedException::class)
        override fun loop() {
            Thread.sleep(20)
        }

        /**
         * Called when the IOIO is disconnected.
         *
         * @see ioio.lib.util.IOIOLooper.disconnected
         */
        override fun disconnected() {
            enableUi(false)
            _midiOutput.setStatus(false)
            _buttonScanner?.setStatus(false)
            _potScanner?.setStatus(false)

            toast("IOIO disconnected")
        }

        /**
         * Called when the IOIO is connected, but has an incompatible firmware version.
         *
         * @see ioio.lib.util.IOIOLooper.incompatible
         */
        override fun incompatible() {
            showVersions(ioio_, "Incompatible firmware version!")
        }
    }

    companion object {
        private const val TAG = "IOIO"
    }
}