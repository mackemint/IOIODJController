package com.example.ioiodjcontroller

class Params
/**
 * The Params file is a place to store all note values and other parameters
 * for the controller.
 *
 */
{
    /**
     * Default set of notes transmitted to the output queue
     */
    val _noteValues = arrayOf(
        intArrayOf(0, 1, 2, 3),
        intArrayOf(4, 5, 6, 7),
        intArrayOf(8, 9, 10, 11),
        intArrayOf(12, 13, 14, 15)
    )

    fun getPotCCValue(row: Int, col: Int): Int {
        return POT_MATRIX1[row][col]
    }

    fun getNoteValues(): Array<IntArray> {
        return _noteValues
    }

    companion object {
        /**
         * Faders and knobs
         */
        private val POT_MATRIX1 = arrayOf(
            intArrayOf(60,61,62,63,64,65),
            intArrayOf(66,67,68,69,70,71),
            intArrayOf(72,73,74,75,76,77)
        )

    }
}
