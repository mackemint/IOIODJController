package com.example.ioiodjcontroller

import android.util.Log

class PotConditioner(initialValue: Float, row: Int) {

    /**
     * Raw float value from A/D conversion
     * TODO Investigate if this needs to be a class variable
     */
    private var _fVal = 0f

    /**
     * Integer value in 7 bit res
     */
    private var _intVal: Int

    /**
     * Previous output value, to prevent MIDI spamming
     */
    private var _prevVal: Int

    init {
        // float value at initialization
        var initial = initialValue
        Log.i(DEBUG_TAG, "Constructor")

        //Correcting wrong wiring of potentiometers
        initial = getRightPolarity(initial, row)

        // Initializing raw values for filtering
        _fVal = initial
        _intVal = adConversion(_fVal)
        _prevVal = 0
    }

    /**
     * Method for returning the right polarity of the pots that I accidentally.
     *
     * @param `val`  analog value
     * @param row  the row 0 and 1 are wrong wired
     *
     * @return value with the right polarity
     */
    private fun getRightPolarity(value: Float, row: Int): Float {
        return if (row < 2) (1 - (value + 0.19f)) else value
    }

    /**
     * This takes a float from 0-1, multiplies it with a constant and returns an integer number
     * @param  input - value from IOIO analog in
     * @param  row - the row of potentiometers to read
     * @return a filtered integer number in 7 bit res
     */
    fun getFilteredInput(input: Float, row: Int): Int {
        var inputValue = input
        inputValue = getRightPolarity(inputValue, row)
        _fVal = inputValue * (1f - F_COEF) + _fVal * F_COEF
        val thisInt = adConversion(_fVal)
        _intVal = (thisInt * (1f - F_COEF) + _intVal * F_COEF).toInt()
        if (_intVal == _prevVal)
            return -1
        _prevVal = _intVal
        return assertVal(_intVal)
    }

    /**
     * Converts a float number from A/D to 7 bit resolution for MIDI implementation
     *
     * @param `val` - filtered analog float val
     * @return an int in 7 bit res
     */
    private fun adConversion(value: Float): Int {
        return (value * MULTIPLIER).toInt()
    }

    /**
     * Assertion that the value is in the right scope
     *
     * @param value filtered analog reading
     *
     * @return a value between 0 - 127
     */
    private fun assertVal(value: Int): Int {
        return when {
            value > MAX_RES -> MAX_RES
            value < 1 -> 0
            else -> value
        }
    }

    companion object {
        private const val MAX_RES = 127
        private const val MULTIPLIER = 156
        private const val DEBUG_TAG = "PotConditioner"

        /**
         * Filter coefficient value
         */
        private const val F_COEF = 0.13f
    }

}
