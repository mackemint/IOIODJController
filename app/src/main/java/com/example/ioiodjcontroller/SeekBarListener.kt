package com.example.ioiodjcontroller

import android.widget.SeekBar

const val MAGIC_SEEK = 50
class SeekBarListener(midiOutput: MIDIOutput): SeekBar.OnSeekBarChangeListener {
    private var _midiOutput = midiOutput
    init {
        _midiOutput = midiOutput
    }
    override fun onProgressChanged(seek: SeekBar,
                                   progress: Int, fromUser: Boolean) {
        if (seek.progress < MAGIC_SEEK)
            _midiOutput.addCcToQueue(Integer.valueOf(seek.tag.toString()), 127+(seek.progress-MAGIC_SEEK))
        else
            _midiOutput.addCcToQueue(Integer.valueOf(seek.tag.toString()), seek.progress-MAGIC_SEEK)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seek: SeekBar) {
        seek.progress = MAGIC_SEEK
    }
}