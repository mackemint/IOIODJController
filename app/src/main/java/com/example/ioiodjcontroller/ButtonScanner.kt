package com.example.ioiodjcontroller

import ioio.lib.api.DigitalInput
import ioio.lib.api.DigitalOutput
import ioio.lib.api.IOIO
import ioio.lib.api.exception.ConnectionLostException


/**
 * Scans hardware buttons and outputs messages to the output queue.
 *
 * @author djdiskmachine
 */
class ButtonScanner(ioio: IOIO, midiOutput: MIDIOutput) : Thread() {
    private val _ioio: IOIO = ioio

    /**
     * An instance of the Main Activity to handle button presses
     */
    private val _midiOutput: MIDIOutput = midiOutput

    /**
     * Saves the current state as the previous one.
     * Used for toggle button feature of H/W buttons
     */
    private val prevReading: Array<BooleanArray> = Array(COL.size) { BooleanArray(ROW.size) }

    /**
     * The columns take in the signal from the outputs
     * Array containing columns of digital inputs
     */
    private val digitIn: Array<DigitalInput?>

    /**
     * Array of pins for digital input
     */
    private val inPin = intArrayOf(COL1_PIN, COL2_PIN, COL3_PIN, COL4_PIN)

    /**
     * The rows output signal
     * Array containing rows of digital outputs
     */
    private val digitOut: Array<DigitalOutput?>

    @Volatile
    private var _running: Boolean

    private val _params: Params = Params()

    /**
     * Default set of notes transmitted to the output queue
     */
    private val MIDI_NOTE_NUMBER: Array<IntArray> = _params.getNoteValues()

    /**
     * A HardwareButton on the keyboard
     */
    private val _hardwareButton: Array<Array<HardwareButton?>>
    override fun run() {
        while (_running) {
            try {
                scanKeyBed()
                sleep(1)
            } catch (e: ConnectionLostException) {
                _running = false
                e.printStackTrace()
            } catch (e: InterruptedException) {
                _running = false
                e.printStackTrace()
            }
        }
    }

    /**
     * Sets running status
     * @param s - the status
     */
    fun setStatus(s: Boolean) {
        _running = s
    }

    /**
     * Scans the button matrix and checks for interconnection between row and column
     * If there is a connection, the corresponding hardware button is pressed.
     *
     *
     * @throws ConnectionLostException
     * @throws InterruptedException
     */
    @Throws(ConnectionLostException::class, InterruptedException::class)
    private fun scanKeyBed() {
        for (j in ROW.indices) {
            digitOut[j]!!.write(true)
            _ioio.sync()
            for (i in COL.indices) {
                _hardwareButton[j][i]?.setCurrentState(digitIn[i]!!.read())
                isButtonToggled(_hardwareButton[j][i])
            }
            digitOut[j]!!.write(false)
        }
    }

    /**
     * Compares hardware buttons' state with previous state.
     * Handles message if state != previousState
     *
     * pre: current button state is set
     * post: previous state is set to current
     *
     * @param  button - hardware button
     * @throws ConnectionLostException
     */
    @Throws(ConnectionLostException::class)
    private fun isButtonToggled(button: HardwareButton?) {

        // Only if state is toggled
        if (button != null) {
            if (button.isPressed || button.isReleased) toggleButton(button)
            button.setPreviousState(button.getCurrentState())
        }
    }

    /**
     * Handler for button toggle events.
     *
     * pre: Button state is toggled
     * post: handleMessage method is called according to conditions
     *
     */
    @Throws(ConnectionLostException::class)
    private fun toggleButton(button: HardwareButton?) {
        val pressed: Boolean = button!!.getCurrentState()
        handleOnButtonPressed(button.getNoteNumber, pressed)
    }

    /**
     * @param number - the MIDI Note message sent to output queue
     * @param keyDown - Note On or Note Off
     *
     * Pre: keyboard modifier is set
     * post: note added to output queue
     */
    private fun handleOnButtonPressed(number: Int, keyDown: Boolean) {
        val noteOn = 1
        val noteOff = 0
        //Log.d("ButtonScanner", "handle message "+number)
        if (keyDown)
            _midiOutput.addNoteToQueue(number, noteOn)
        else
            _midiOutput.addNoteToQueue(number, noteOff)
    }

    /**
     * Creates a new ButtonScanner with knowledge of main activity components.
     */
    init {
        digitIn = arrayOfNulls(inPin.size)
        digitOut = arrayOfNulls(OUT_PIN.size)
        _hardwareButton = Array(COL.size) { arrayOfNulls(ROW.size) }
        _running = true

        for (i in COL.indices) {

            //			Log.i(DEBUG_TAG, "length i is: " + i);
            /**
             * As a workaround for the async bug, only the output is opened here
             */
            digitOut[i] = _ioio.openDigitalOutput(OUT_PIN[i], false)
            digitIn[i] = _ioio.openDigitalInput(inPin[i], DigitalInput.Spec.Mode.PULL_DOWN)
            /**
             * Initializing the array of previous readings
             */
            for (j in prevReading.indices) {
                _hardwareButton[i][j] = HardwareButton(MIDI_NOTE_NUMBER[i][j])
                prevReading[i][j] = false
            }
        }

    }

    companion object {
        /**
         * Row pin numbers
         */
        private const val ROW1_PIN = 18
        private const val ROW2_PIN = 19
        private const val ROW3_PIN = 20
        private const val ROW4_PIN = 21

        /**
         * Row pin numbers
         */
        private const val COL1_PIN = 25
        private const val COL2_PIN = 24
        private const val COL3_PIN = 23
        private const val COL4_PIN = 22

        /**
         * Array of pins for digital output
         */
        private val OUT_PIN = intArrayOf(
            ROW1_PIN, ROW2_PIN,
            ROW3_PIN, ROW4_PIN
        )
        /**
         * Array of booleans used for storing previous states
         * Columns take input signal
         */
        private val COL: BooleanArray = BooleanArray(OUT_PIN.size)

        /**
         * Array of booleans used for storing previous states
         * Rows output signal
         */
        private val ROW: BooleanArray = BooleanArray(OUT_PIN.size)

    }
}


